# elk stack practice

## Ubuntu 18.04 VM - elasticsearch + logstash + kibana
- create via rsyslog sshd log file (see rsyslog config in rsyslog_sshd.conf)
- set in ssh daemon config debug level = INFO (to produce more messages)
- set logstash pipeline (see in logstash_sshd.conf)
- discover index 'logs-generic-default*' and show messages (see in screenshot 'Discover-Elastic.png')